﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ClipEditor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.lstClips = New System.Windows.Forms.ListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblClipID = New System.Windows.Forms.Label()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.NumericUpDownReq = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtClipCode = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblDrawingID = New System.Windows.Forms.Label()
        Me.btnDrawing = New System.Windows.Forms.Button()
        Me.txtDrawing = New System.Windows.Forms.TextBox()
        Me.rbDrawing = New System.Windows.Forms.RadioButton()
        Me.rbSystem = New System.Windows.Forms.RadioButton()
        Me.lstPipes = New System.Windows.Forms.ComboBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.NumericUpDownReq, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(416, 331)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.btnDelete)
        Me.Panel2.Controls.Add(Me.lstClips)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Location = New System.Drawing.Point(321, 43)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(241, 267)
        Me.Panel2.TabIndex = 10
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(146, 236)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'lstClips
        '
        Me.lstClips.FormattingEnabled = True
        Me.lstClips.Location = New System.Drawing.Point(20, 30)
        Me.lstClips.Name = "lstClips"
        Me.lstClips.Size = New System.Drawing.Size(201, 186)
        Me.lstClips.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(20, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Existing Clips"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblClipID)
        Me.Panel1.Controls.Add(Me.btnUpdate)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.NumericUpDownReq)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtClipCode)
        Me.Panel1.Location = New System.Drawing.Point(21, 206)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(271, 104)
        Me.Panel1.TabIndex = 9
        '
        'lblClipID
        '
        Me.lblClipID.AutoSize = True
        Me.lblClipID.Location = New System.Drawing.Point(180, 10)
        Me.lblClipID.Name = "lblClipID"
        Me.lblClipID.Size = New System.Drawing.Size(38, 13)
        Me.lblClipID.TabIndex = 6
        Me.lblClipID.Text = "Clip ID"
        Me.lblClipID.Visible = False
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(96, 73)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 5
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(177, 73)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(20, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Number/m"
        '
        'NumericUpDownReq
        '
        Me.NumericUpDownReq.Location = New System.Drawing.Point(23, 76)
        Me.NumericUpDownReq.Name = "NumericUpDownReq"
        Me.NumericUpDownReq.Size = New System.Drawing.Size(41, 20)
        Me.NumericUpDownReq.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(24, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Clip"
        '
        'txtClipCode
        '
        Me.txtClipCode.Location = New System.Drawing.Point(20, 29)
        Me.txtClipCode.Name = "txtClipCode"
        Me.txtClipCode.Size = New System.Drawing.Size(232, 20)
        Me.txtClipCode.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Pipe Code"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.lblDrawingID)
        Me.Panel3.Controls.Add(Me.btnDrawing)
        Me.Panel3.Controls.Add(Me.txtDrawing)
        Me.Panel3.Controls.Add(Me.rbDrawing)
        Me.Panel3.Controls.Add(Me.rbSystem)
        Me.Panel3.Location = New System.Drawing.Point(21, 81)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(271, 107)
        Me.Panel3.TabIndex = 11
        '
        'lblDrawingID
        '
        Me.lblDrawingID.AutoSize = True
        Me.lblDrawingID.Location = New System.Drawing.Point(177, 18)
        Me.lblDrawingID.Name = "lblDrawingID"
        Me.lblDrawingID.Size = New System.Drawing.Size(60, 13)
        Me.lblDrawingID.TabIndex = 4
        Me.lblDrawingID.Text = "Drawing ID"
        Me.lblDrawingID.Visible = False
        '
        'btnDrawing
        '
        Me.btnDrawing.Enabled = False
        Me.btnDrawing.Location = New System.Drawing.Point(223, 65)
        Me.btnDrawing.Name = "btnDrawing"
        Me.btnDrawing.Size = New System.Drawing.Size(29, 19)
        Me.btnDrawing.TabIndex = 3
        Me.btnDrawing.Text = "..."
        Me.btnDrawing.UseVisualStyleBackColor = True
        '
        'txtDrawing
        '
        Me.txtDrawing.Enabled = False
        Me.txtDrawing.Location = New System.Drawing.Point(20, 65)
        Me.txtDrawing.Name = "txtDrawing"
        Me.txtDrawing.Size = New System.Drawing.Size(196, 20)
        Me.txtDrawing.TabIndex = 2
        '
        'rbDrawing
        '
        Me.rbDrawing.AutoSize = True
        Me.rbDrawing.Location = New System.Drawing.Point(20, 42)
        Me.rbDrawing.Name = "rbDrawing"
        Me.rbDrawing.Size = New System.Drawing.Size(82, 17)
        Me.rbDrawing.TabIndex = 1
        Me.rbDrawing.Text = "For Drawing"
        Me.rbDrawing.UseVisualStyleBackColor = True
        '
        'rbSystem
        '
        Me.rbSystem.AutoSize = True
        Me.rbSystem.Checked = True
        Me.rbSystem.Location = New System.Drawing.Point(20, 18)
        Me.rbSystem.Name = "rbSystem"
        Me.rbSystem.Size = New System.Drawing.Size(96, 17)
        Me.rbSystem.TabIndex = 0
        Me.rbSystem.TabStop = True
        Me.rbSystem.Text = "System Default"
        Me.rbSystem.UseVisualStyleBackColor = True
        '
        'lstPipes
        '
        Me.lstPipes.FormattingEnabled = True
        Me.lstPipes.Location = New System.Drawing.Point(25, 43)
        Me.lstPipes.Name = "lstPipes"
        Me.lstPipes.Size = New System.Drawing.Size(121, 21)
        Me.lstPipes.TabIndex = 12
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ClipEditor
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(574, 372)
        Me.Controls.Add(Me.lstPipes)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ClipEditor"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Clip Editor"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.NumericUpDownReq, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents lstClips As System.Windows.Forms.ListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDownReq As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtClipCode As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnDrawing As System.Windows.Forms.Button
    Friend WithEvents txtDrawing As System.Windows.Forms.TextBox
    Friend WithEvents rbDrawing As System.Windows.Forms.RadioButton
    Friend WithEvents rbSystem As System.Windows.Forms.RadioButton
    Friend WithEvents lstPipes As System.Windows.Forms.ComboBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lblDrawingID As System.Windows.Forms.Label
    Friend WithEvents lblClipID As System.Windows.Forms.Label

End Class
