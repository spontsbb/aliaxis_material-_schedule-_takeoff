﻿Imports System.Windows.Forms

Public Class AddAssembly
    Dim asm As New Aspect.MarleyAssembly(dbManager.sqlconnectionString)

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Dim asmID As Long = asm.AddAssembly(txtAssemblyCode.Text)
        If chkCopy.Checked Then
            asm.copyAssemblyComponents(cboAssemblies.SelectedItem!ID, asmID)
        End If
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub AddAssembly_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboAssemblies.DataSource = asm.getAssemblies()
        cboAssemblies.DisplayMember = "AssemblyCode"

    End Sub
End Class
