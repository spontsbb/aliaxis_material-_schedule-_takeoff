﻿Imports System.Xml

Public Class dbManager
    Public Shared Property dbLocation() As String
        Get
            Dim strDB As String = My.Settings.PartsDataLocation
            Dim pos As Integer = strDB.IndexOf("Data") + 13
            If strDB = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" Then
                strDB = ""

            Else

                strDB = strDB.Substring(pos, strDB.Substring(pos).Length - 1)
            End If
            Return strDB

        End Get
        Set(ByVal value As String)
            Dim strDB As String = My.Settings.PartsDataLocation
            Dim pos As Integer = strDB.IndexOf("Data")
            strDB = strDB.Substring(0, pos) & "Data Source='" & value & "'"
            My.Settings.PartsDataLocation = strDB
            My.Settings.Save()
        End Set
    End Property
    Public Shared Property sqldbLocation() As String
        Get
            Dim strDB As String = My.Settings.sqlPartsDataLocation


            Return strDB

        End Get
        Set(ByVal value As String)
            Dim strDB As String = My.Settings.sqlPartsDataLocation

            My.Settings.sqlPartsDataLocation = strDB
            My.Settings.Save()
        End Set
    End Property
    Public Shared Property Logo() As String
        Get
            Dim strDB As String = My.Settings.Logo
            
            Return strDB

        End Get
        Set(ByVal value As String)
            Dim strDB As String = My.Settings.Logo
            My.Settings.Logo = strDB
            My.Settings.Save()
        End Set
    End Property
    Public Shared ReadOnly Property connectionString() As String
        Get
            Return My.Settings.PartsDataLocation
        End Get
    End Property


    Public Shared ReadOnly Property sqlconnectionString() As String
        Get
            Return My.Settings.sqlPartsDataLocation
        End Get
    End Property





End Class
