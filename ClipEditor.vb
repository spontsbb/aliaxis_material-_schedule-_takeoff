﻿Imports System.Windows.Forms

Public Class ClipEditor
    Private mc As New MarleyPipe(dbManager.sqlconnectionString)
    Private forDrawing As Integer = 1
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        mc.SaveClips()
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub rbSystem_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSystem.CheckedChanged
        txtDrawing.Enabled = rbDrawing.Checked
        btnDrawing.Enabled = rbDrawing.Checked
        If rbSystem.Checked Then
            forDrawing = 1
            If lstPipes.SelectedIndex >= 0 Then
                lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!id, forDrawing)
            End If
        ElseIf lblDrawingID.Text <> "Drawing ID" Then
            forDrawing = lblDrawingID.Text
            lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!id, forDrawing)
        Else
            lstClips.DataSource = Nothing
        End If
    End Sub

    Private Sub ClipEditor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lstPipes.DataSource = mc.getPipes
        lstPipes.DisplayMember = "Pipecode"
    End Sub

    Private Sub btnDrawing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrawing.Click

        Dim cad As New MicroGDS(dbManager.sqlconnectionString)
        txtDrawing.Text = cad.getDrawingFileName
        Dim drawingID As Long = mc.getDrawingID(txtDrawing.Text)
        If drawingID = -1 Then
            lblDrawingID.Text = mc.addDrawing(txtDrawing.Text)

        Else
            lblDrawingID.Text = drawingID
        End If
        forDrawing = lblDrawingID.Text
        lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!id, forDrawing)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim frm As New AddPipeClip
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim cd As ClipDetails
            If rbSystem.Checked Then
                cd.Drawing = 1
            Else
                cd.Drawing = lblDrawingID.Text
            End If
            cd.No = frm.NumericUpDownReq.Value
            cd.partCode = frm.txtClipCode.Text
            cd.PipeID = lstPipes.SelectedItem!ID
            mc.AddPipeClip(cd)
            lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!id, forDrawing)
            lstClips.SelectedIndex = lstClips.FindString(cd.partCode)
        End If

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim cd As ClipDetails
        If rbSystem.Checked Then
            cd.Drawing = 1
        Else
            cd.Drawing = lblDrawingID.Text
        End If
        cd.No = NumericUpDownReq.Value
        cd.partCode = txtClipCode.Text
        cd.ClipID = lblClipID.Text
        cd.PipeID = lstPipes.SelectedItem!id
        mc.UpdatePipeClips(cd)
        'lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!id, forDrawing)
        lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!pipecode, forDrawing)
        lstClips.SelectedItem = cd
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim cd As ClipDetails = lstClips.SelectedItem
        mc.deleteClip(cd.ClipID)
        ' lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!id, forDrawing)
        lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!pipecode, forDrawing)
    End Sub

    Private Sub lstPipes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstPipes.SelectedIndexChanged

        'If lstPipes.SelectedIndex <> 0 Then

        lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!pipecode, forDrawing)
        ' lstClips.DataSource = mc.getPipeClips(lstPipes.SelectedItem!pipeid, forDrawing)

        ' End If

    End Sub

    Private Sub lstClips_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstClips.SelectedIndexChanged
        Dim cd As ClipDetails = lstClips.SelectedItem
        txtClipCode.Text = cd.partCode
        NumericUpDownReq.Value = cd.No
        lblClipID.Text = cd.ClipID
    End Sub

    Private Sub rbDrawing_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbDrawing.CheckedChanged

    End Sub
End Class
