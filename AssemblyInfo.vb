Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices


' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Material Schedule 2021")>
<Assembly: AssemblyDescription("Material Schedule 2021")>
<Assembly: AssemblyCompany("@spect Training and CADline")>
<Assembly: AssemblyProduct("Material Schedule 2021")>
<Assembly: AssemblyCopyright("(c) @spect Training 2011 & Cadline")>
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 


'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("5BCCE05A-105E-42B4-90B2-D49870E4795D")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("2.0.0.0")>

<Assembly: ComVisibleAttribute(False)>
<Assembly: AssemblyFileVersion("2.0.0.0")>