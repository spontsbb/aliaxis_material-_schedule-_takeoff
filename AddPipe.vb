﻿Imports System.Windows.Forms

Public Class AddPipe

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Dim mp As New Aspect.MarleyPipe(dbManager.sqlconnectionString)
        Dim pd As Aspect.PipeDetails
        pd.pipeCode = txtPipeCode.Text
        pd.pipeDia = txtPipeDia.Text
        pd.unitLength = NumericUpDownLength.Value
        mp.AddPipe(pd)
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

End Class
