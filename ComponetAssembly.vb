﻿Imports System.Windows.Forms

Public Class ComponetAssembly
    Private asm As New Aspect.MarleyAssembly(dbManager.sqlconnectionString)
    Dim copyAssemply As Collections.Generic.List(Of Aspect.AssemblyDetails)
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        asm.Save()
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ComponetAssembly_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lstAssembly.DataSource = asm.getAssemblies
        lstAssembly.DisplayMember = "AssemblyCode"

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim frm As New AddAssemblyComponent
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim ad As Aspect.AssemblyDetails
            ad.partCode = frm.txtComponentCode.Text

            If frm.chkPipe.Checked Then
                ad.No = frm.txtLength.Text
            Else
                ad.No = frm.NumericUpDownNoReq.Value
            End If

            ad.AssemblyID = lstAssembly.SelectedItem!id
            ad.isPipe = frm.chkPipe.Checked
            asm.AddComponent(ad)
            lstComponents.DataSource = asm.getAssemblyParts_by_ID(lstAssembly.SelectedItem!id)
            lstComponents.SelectedIndex = lstComponents.FindString(ad.partCode)


        End If
    End Sub

    Private Sub lstAssembly_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstAssembly.SelectedIndexChanged

        For i As Integer = 0 To lstComponents.Items.Count - 1
            lstComponents.SetItemChecked(i, False)
        Next


        lstComponents.DataSource = asm.getAssemblyParts_by_ID(lstAssembly.SelectedItem!id)
    End Sub

    

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim ad As Aspect.AssemblyDetails
        ad.partCode = txtComponent.Text
        If chkPipe.Checked Then
            ad.No = txtLength.Text
        Else
            ad.No = NumericUpDownNoOff.Value
        End If

        ad.AssemblyID = lstAssembly.SelectedItem!id
        ad.partID = LabelID.Text
        ad.isPipe = chkPipe.Checked
        asm.UpdateComponent(ad)
        lstComponents.DataSource = asm.getAssemblyParts_by_ID(lstAssembly.SelectedItem!id)
        lstComponents.SelectedItem = ad
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        For Each ad As Aspect.AssemblyDetails In lstComponents.CheckedItems
            asm.deleteComponent(ad.partID)
        Next

        lstComponents.DataSource = asm.getAssemblyParts_by_ID(lstAssembly.SelectedItem!id)
    End Sub

    Private Sub chkPipe_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPipe.CheckedChanged
        resetForPipe()
    End Sub

    Private Sub resetForPipe()
        If chkPipe.Checked Then
            lblNo.Text = "Length"
            NumericUpDownNoOff.Visible = False
            txtLength.Visible = True
        Else
            lblNo.Text = "Number"
            NumericUpDownNoOff.Visible = True
            txtLength.Visible = False
        End If
    End Sub

    Private Sub lstComponents_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstComponents.SelectedIndexChanged
        Dim ad As Aspect.AssemblyDetails = lstComponents.SelectedItem
        LabelID.Text = ad.partID
        txtComponent.Text = ad.partCode

        NumericUpDownNoOff.Value = ad.No
        txtLength.Text = ad.No
        chkPipe.Checked = ad.isPipe
        resetForPipe()
    End Sub

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        copyAssemply = New Collections.Generic.List(Of Aspect.AssemblyDetails)

        For Each ad As Aspect.AssemblyDetails In lstComponents.CheckedItems
            copyAssemply.Add(ad)
        Next

    End Sub

    Private Sub btnPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPaste.Click
        For Each ad As Aspect.AssemblyDetails In copyAssemply
            ad.AssemblyID = lstAssembly.SelectedItem!id
            asm.AddComponent(ad)
        Next

        lstComponents.DataSource = asm.getAssemblyParts_by_ID(lstAssembly.SelectedItem!id)

    End Sub
End Class
