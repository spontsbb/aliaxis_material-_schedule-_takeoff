﻿Imports System.Windows.Forms

Public Class SwopSystem

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub SwopSystem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sw As New MarleySwopSystem(dbManager.sqlconnectionString)
        lstFromSystem.DataSource = sw.getSystemsList
        lstToSystem.DataSource = sw.getSystemsList
    End Sub
End Class
