﻿Imports System.Windows.Forms

Public Class DeleteMaterialSwopSystem
    Public messageHeader As String
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If MessageBox.Show("This will delete " & lstSystems.SelectedItem & " and all assoccated parts" & vbCrLf & "Do you want to continue", messageHeader, MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) = Windows.Forms.DialogResult.OK Then
            Dim sw As New MarleySwopSystem(dbManager.sqlconnectionString)
            sw.DeleteSystem(lstSystems.SelectedItem)
        End If

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DeleteMaterialSwopSystem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sw As New MarleySwopSystem(dbManager.sqlconnectionString)
        lstSystems.DataSource = sw.getSystemsList

    End Sub
End Class
