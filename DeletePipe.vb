﻿Imports System.Windows.Forms

Public Class DeletePipe
    Private pd As New MarleyPipe(dbManager.sqlconnectionString)
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
       
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DeletePipe_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        lstPipes.DataSource = pd.getPipes
        lstPipes.DisplayMember = "pipeCode"
    End Sub
End Class
