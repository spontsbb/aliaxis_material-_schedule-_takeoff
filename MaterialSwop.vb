﻿Imports System.Windows.Forms

Public Class MaterialSwop
    Private sw As New Aspect.MarleySwopSystem(dbManager.sqlconnectionString)
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        sw.save()
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub MaterialSwop_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        sw = New Aspect.MarleySwopSystem(dbManager.sqlconnectionString)
        Dim dv As DataView = sw.GetSwopSystems

        ' dann = sw.getswopsystem_dataadapter()

        DataGridView1.DataSource = dv
        DataGridView1.Columns(0).Visible = False
    End Sub
End Class
