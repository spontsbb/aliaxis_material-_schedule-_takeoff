﻿Imports System.Windows.Forms

Public Class DeleteAssembly
    Dim ma As New MarleyAssembly(dbManager.sqlconnectionString)
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Dim strMessage As String = ""

        If lstAssembly.CheckedItems.Count = 0 Then
            Exit Sub
        ElseIf lstAssembly.CheckedItems.Count > 1 Then

            strMessage = "You are about to delete " & lstAssembly.CheckedItems.Count & " assemblies"
        Else

            strMessage = "You are about to delete Assembly " & lstAssembly.CheckedItems(0)!assemblycode
        End If

        If MessageBox.Show(strMessage & vbCrLf & "Are you sure", "Title", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) Then
            Dim toDelete As New Generic.List(Of Long)
            For Each item As DataRowView In lstAssembly.CheckedItems

                toDelete.Add(item!ID)
            Next
            For Each id As Long In toDelete
                ma.deleteAssembly(id)
            Next
        End If
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DeleteAssembly_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lstAssembly.DataSource = ma.getAssemblies
        lstAssembly.DisplayMember = "AssemblyCode"
    End Sub
End Class
