
Imports System.data
Imports System.IO
Imports System.Drawing.printing
Imports System.xml
Public Class Main
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSave As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuBoxScan As System.Windows.Forms.MenuItem
    Friend WithEvents mnuScanAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNoOff As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSaveAs As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.MenuItem
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents mnuScanAllDrawings As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPartsDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAspect As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAssembly As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemClipEditor As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAddSwopSystem As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemMaterialSwopEditor As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemSwopSystem As System.Windows.Forms.MenuItem
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButtonOpen As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonPrint As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButtonScanArea As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonScanDrawing As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonScanAllDrawings As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButtonSetNoOff As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButtonAddMatSwopSys As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonMatSwopEditor As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonSwopSystem As System.Windows.Forms.ToolStripButton
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabelTotal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents MenuItemDeleteMaterialSystem As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAddAssembly As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAddPipe As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemDeleteAssembly As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemDeletePipe As System.Windows.Forms.MenuItem
    Friend WithEvents ToolStripDropDownButton1 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents AddAssemblyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteAssemblyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AssemblyEditorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripButtonPipeClipEditor As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents AddPipeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeletePipeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PipeClipEditorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MenuItemGetDrgAssembliess As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPriceUpdate As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.mnuOpen = New System.Windows.Forms.MenuItem()
        Me.mnuSave = New System.Windows.Forms.MenuItem()
        Me.mnuSaveAs = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.mnuPrint = New System.Windows.Forms.MenuItem()
        Me.MenuItem9 = New System.Windows.Forms.MenuItem()
        Me.mnuExit = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.mnuBoxScan = New System.Windows.Forms.MenuItem()
        Me.mnuScanAll = New System.Windows.Forms.MenuItem()
        Me.mnuScanAllDrawings = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItemAddAssembly = New System.Windows.Forms.MenuItem()
        Me.MenuItemDeleteAssembly = New System.Windows.Forms.MenuItem()
        Me.MenuItemAssembly = New System.Windows.Forms.MenuItem()
        Me.MenuItemGetDrgAssembliess = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.MenuItemAddPipe = New System.Windows.Forms.MenuItem()
        Me.MenuItemDeletePipe = New System.Windows.Forms.MenuItem()
        Me.MenuItemClipEditor = New System.Windows.Forms.MenuItem()
        Me.MenuItem14 = New System.Windows.Forms.MenuItem()
        Me.MenuItemAddSwopSystem = New System.Windows.Forms.MenuItem()
        Me.MenuItemDeleteMaterialSystem = New System.Windows.Forms.MenuItem()
        Me.MenuItemMaterialSwopEditor = New System.Windows.Forms.MenuItem()
        Me.MenuItemSwopSystem = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuNoOff = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuPartsDB = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.mnuPriceUpdate = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.mnuAspect = New System.Windows.Forms.MenuItem()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabelTotal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ToolStripButtonOpen = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonSave = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonPrint = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonScanArea = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonScanDrawing = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonScanAllDrawings = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonSetNoOff = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.AddAssemblyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteAssemblyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AssemblyEditorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripButtonPipeClipEditor = New System.Windows.Forms.ToolStripDropDownButton()
        Me.AddPipeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeletePipeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PipeClipEditorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripButtonAddMatSwopSys = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonMatSwopEditor = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonSwopSystem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem11, Me.MenuItem13, Me.MenuItem14, Me.MenuItem6, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuOpen, Me.mnuSave, Me.mnuSaveAs, Me.MenuItem5, Me.mnuPrint, Me.MenuItem9, Me.mnuExit})
        Me.MenuItem1.Text = "&File"
        '
        'mnuOpen
        '
        Me.mnuOpen.Index = 0
        Me.mnuOpen.Shortcut = System.Windows.Forms.Shortcut.CtrlO
        Me.mnuOpen.Text = "&Open"
        '
        'mnuSave
        '
        Me.mnuSave.Enabled = False
        Me.mnuSave.Index = 1
        Me.mnuSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS
        Me.mnuSave.Text = "&Save"
        '
        'mnuSaveAs
        '
        Me.mnuSaveAs.Enabled = False
        Me.mnuSaveAs.Index = 2
        Me.mnuSaveAs.Text = "Save &As"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'mnuPrint
        '
        Me.mnuPrint.Enabled = False
        Me.mnuPrint.Index = 4
        Me.mnuPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP
        Me.mnuPrint.Text = "&Print"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 5
        Me.MenuItem9.Text = "-"
        '
        'mnuExit
        '
        Me.mnuExit.Index = 6
        Me.mnuExit.Text = "E&xit"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuBoxScan, Me.mnuScanAll, Me.mnuScanAllDrawings})
        Me.MenuItem2.Text = "&Scan"
        '
        'mnuBoxScan
        '
        Me.mnuBoxScan.Index = 0
        Me.mnuBoxScan.Shortcut = System.Windows.Forms.Shortcut.CtrlA
        Me.mnuBoxScan.Text = "&Scan Area"
        '
        'mnuScanAll
        '
        Me.mnuScanAll.Index = 1
        Me.mnuScanAll.Shortcut = System.Windows.Forms.Shortcut.CtrlD
        Me.mnuScanAll.Text = "Scan &Drawing"
        '
        'mnuScanAllDrawings
        '
        Me.mnuScanAllDrawings.Index = 2
        Me.mnuScanAllDrawings.Text = "Scan &All Drawings"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 2
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemAddAssembly, Me.MenuItemDeleteAssembly, Me.MenuItemAssembly, Me.MenuItemGetDrgAssembliess})
        Me.MenuItem11.Text = "&Assemblies"
        '
        'MenuItemAddAssembly
        '
        Me.MenuItemAddAssembly.Index = 0
        Me.MenuItemAddAssembly.Text = "Add Assembly"
        '
        'MenuItemDeleteAssembly
        '
        Me.MenuItemDeleteAssembly.Index = 1
        Me.MenuItemDeleteAssembly.Text = "Delete Assembly"
        '
        'MenuItemAssembly
        '
        Me.MenuItemAssembly.Index = 2
        Me.MenuItemAssembly.Text = "Assembly Editor"
        '
        'MenuItemGetDrgAssembliess
        '
        Me.MenuItemGetDrgAssembliess.Index = 3
        Me.MenuItemGetDrgAssembliess.Text = "Get Drawing Assemblies"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 3
        Me.MenuItem13.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemAddPipe, Me.MenuItemDeletePipe, Me.MenuItemClipEditor})
        Me.MenuItem13.Text = "&Pipes"
        '
        'MenuItemAddPipe
        '
        Me.MenuItemAddPipe.Index = 0
        Me.MenuItemAddPipe.Text = "Add Pipe"
        '
        'MenuItemDeletePipe
        '
        Me.MenuItemDeletePipe.Index = 1
        Me.MenuItemDeletePipe.Text = "Delete Pipe"
        '
        'MenuItemClipEditor
        '
        Me.MenuItemClipEditor.Index = 2
        Me.MenuItemClipEditor.Text = "Pipe Clip Editor"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 4
        Me.MenuItem14.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemAddSwopSystem, Me.MenuItemDeleteMaterialSystem, Me.MenuItemMaterialSwopEditor, Me.MenuItemSwopSystem})
        Me.MenuItem14.Text = "&Material Swop"
        '
        'MenuItemAddSwopSystem
        '
        Me.MenuItemAddSwopSystem.Index = 0
        Me.MenuItemAddSwopSystem.Text = "Add Material Swop System"
        '
        'MenuItemDeleteMaterialSystem
        '
        Me.MenuItemDeleteMaterialSystem.Index = 1
        Me.MenuItemDeleteMaterialSystem.Text = "Delete Material Swop System"
        '
        'MenuItemMaterialSwopEditor
        '
        Me.MenuItemMaterialSwopEditor.Index = 2
        Me.MenuItemMaterialSwopEditor.Text = "Material Swop Editor"
        '
        'MenuItemSwopSystem
        '
        Me.MenuItemSwopSystem.Index = 3
        Me.MenuItemSwopSystem.Text = "Swop System"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 5
        Me.MenuItem6.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNoOff, Me.MenuItem4, Me.mnuPartsDB, Me.MenuItem7, Me.mnuPriceUpdate})
        Me.MenuItem6.Text = "&Tools"
        '
        'mnuNoOff
        '
        Me.mnuNoOff.Index = 0
        Me.mnuNoOff.Text = "Set &No. Off"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuPartsDB
        '
        Me.mnuPartsDB.Index = 2
        Me.mnuPartsDB.Text = "Set Connection String"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 3
        Me.MenuItem7.Text = "-"
        Me.MenuItem7.Visible = False
        '
        'mnuPriceUpdate
        '
        Me.mnuPriceUpdate.Index = 4
        Me.mnuPriceUpdate.Text = "&Update Price Data"
        Me.mnuPriceUpdate.Visible = False
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 6
        Me.MenuItem3.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAbout, Me.mnuAspect})
        Me.MenuItem3.Text = "&Help"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 0
        Me.mnuAbout.Shortcut = System.Windows.Forms.Shortcut.F1
        Me.mnuAbout.Text = "&About"
        '
        'mnuAspect
        '
        Me.mnuAspect.Index = 1
        Me.mnuAspect.Text = "Contact Aspect Training"
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(536, 316)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(64, 16)
        Me.lblTotal.TabIndex = 4
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonOpen, Me.ToolStripButtonSave, Me.ToolStripButtonPrint, Me.ToolStripSeparator1, Me.ToolStripButtonScanArea, Me.ToolStripButtonScanDrawing, Me.ToolStripButtonScanAllDrawings, Me.ToolStripSeparator2, Me.ToolStripButtonSetNoOff, Me.ToolStripSeparator3, Me.ToolStripDropDownButton1, Me.ToolStripSeparator5, Me.ToolStripButtonPipeClipEditor, Me.ToolStripSeparator4, Me.ToolStripButtonAddMatSwopSys, Me.ToolStripButtonMatSwopEditor, Me.ToolStripButtonSwopSystem})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(624, 25)
        Me.ToolStrip1.TabIndex = 5
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabelTotal})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 319)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(624, 22)
        Me.StatusStrip1.TabIndex = 6
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(46, 17)
        Me.ToolStripStatusLabel1.Text = "Total:   "
        '
        'ToolStripStatusLabelTotal
        '
        Me.ToolStripStatusLabelTotal.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabelTotal.Name = "ToolStripStatusLabelTotal"
        Me.ToolStripStatusLabelTotal.Size = New System.Drawing.Size(41, 17)
        Me.ToolStripStatusLabelTotal.Text = "� 0.00"
        Me.ToolStripStatusLabelTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DataGridView1
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(0, 52)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(624, 264)
        Me.DataGridView1.TabIndex = 7
        '
        'ToolStripButtonOpen
        '
        Me.ToolStripButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonOpen.Image = CType(resources.GetObject("ToolStripButtonOpen.Image"), System.Drawing.Image)
        Me.ToolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonOpen.Name = "ToolStripButtonOpen"
        Me.ToolStripButtonOpen.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonOpen.ToolTipText = "Open Schedule"
        '
        'ToolStripButtonSave
        '
        Me.ToolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonSave.Enabled = False
        Me.ToolStripButtonSave.Image = CType(resources.GetObject("ToolStripButtonSave.Image"), System.Drawing.Image)
        Me.ToolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonSave.Name = "ToolStripButtonSave"
        Me.ToolStripButtonSave.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonSave.Text = "ToolStripButtonSave"
        Me.ToolStripButtonSave.ToolTipText = "Save Schedule"
        '
        'ToolStripButtonPrint
        '
        Me.ToolStripButtonPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonPrint.Enabled = False
        Me.ToolStripButtonPrint.Image = CType(resources.GetObject("ToolStripButtonPrint.Image"), System.Drawing.Image)
        Me.ToolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonPrint.Name = "ToolStripButtonPrint"
        Me.ToolStripButtonPrint.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonPrint.Text = "ToolStripButtonPrint"
        Me.ToolStripButtonPrint.ToolTipText = "Print Schedule"
        '
        'ToolStripButtonScanArea
        '
        Me.ToolStripButtonScanArea.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonScanArea.Image = CType(resources.GetObject("ToolStripButtonScanArea.Image"), System.Drawing.Image)
        Me.ToolStripButtonScanArea.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonScanArea.Name = "ToolStripButtonScanArea"
        Me.ToolStripButtonScanArea.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonScanArea.Text = "ToolStripButton4"
        Me.ToolStripButtonScanArea.ToolTipText = "Scan Area"
        '
        'ToolStripButtonScanDrawing
        '
        Me.ToolStripButtonScanDrawing.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonScanDrawing.Image = CType(resources.GetObject("ToolStripButtonScanDrawing.Image"), System.Drawing.Image)
        Me.ToolStripButtonScanDrawing.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonScanDrawing.Name = "ToolStripButtonScanDrawing"
        Me.ToolStripButtonScanDrawing.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonScanDrawing.Text = "ToolStripButton5"
        Me.ToolStripButtonScanDrawing.ToolTipText = "Scan Drawing"
        '
        'ToolStripButtonScanAllDrawings
        '
        Me.ToolStripButtonScanAllDrawings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonScanAllDrawings.Image = CType(resources.GetObject("ToolStripButtonScanAllDrawings.Image"), System.Drawing.Image)
        Me.ToolStripButtonScanAllDrawings.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonScanAllDrawings.Name = "ToolStripButtonScanAllDrawings"
        Me.ToolStripButtonScanAllDrawings.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonScanAllDrawings.Text = "ToolStripButton6"
        Me.ToolStripButtonScanAllDrawings.ToolTipText = "Scan All Drawings"
        '
        'ToolStripButtonSetNoOff
        '
        Me.ToolStripButtonSetNoOff.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonSetNoOff.Image = CType(resources.GetObject("ToolStripButtonSetNoOff.Image"), System.Drawing.Image)
        Me.ToolStripButtonSetNoOff.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonSetNoOff.Name = "ToolStripButtonSetNoOff"
        Me.ToolStripButtonSetNoOff.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonSetNoOff.Text = "ToolStripButtonSetNoOff"
        Me.ToolStripButtonSetNoOff.ToolTipText = "Set Number Off"
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripDropDownButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddAssemblyToolStripMenuItem, Me.DeleteAssemblyToolStripMenuItem, Me.AssemblyEditorToolStripMenuItem})
        Me.ToolStripDropDownButton1.Image = CType(resources.GetObject("ToolStripDropDownButton1.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(29, 22)
        Me.ToolStripDropDownButton1.Text = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.ToolTipText = "Assemblies"
        '
        'AddAssemblyToolStripMenuItem
        '
        Me.AddAssemblyToolStripMenuItem.Name = "AddAssemblyToolStripMenuItem"
        Me.AddAssemblyToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AddAssemblyToolStripMenuItem.Text = "Add Assembly"
        '
        'DeleteAssemblyToolStripMenuItem
        '
        Me.DeleteAssemblyToolStripMenuItem.Name = "DeleteAssemblyToolStripMenuItem"
        Me.DeleteAssemblyToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.DeleteAssemblyToolStripMenuItem.Text = "Delete Assembly"
        '
        'AssemblyEditorToolStripMenuItem
        '
        Me.AssemblyEditorToolStripMenuItem.Name = "AssemblyEditorToolStripMenuItem"
        Me.AssemblyEditorToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.AssemblyEditorToolStripMenuItem.Text = "Assembly Editor"
        '
        'ToolStripButtonPipeClipEditor
        '
        Me.ToolStripButtonPipeClipEditor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonPipeClipEditor.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddPipeToolStripMenuItem, Me.DeletePipeToolStripMenuItem, Me.PipeClipEditorToolStripMenuItem})
        Me.ToolStripButtonPipeClipEditor.Image = CType(resources.GetObject("ToolStripButtonPipeClipEditor.Image"), System.Drawing.Image)
        Me.ToolStripButtonPipeClipEditor.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonPipeClipEditor.Name = "ToolStripButtonPipeClipEditor"
        Me.ToolStripButtonPipeClipEditor.Size = New System.Drawing.Size(29, 22)
        Me.ToolStripButtonPipeClipEditor.Text = "ToolStripButton9"
        Me.ToolStripButtonPipeClipEditor.ToolTipText = "Pipes"
        '
        'AddPipeToolStripMenuItem
        '
        Me.AddPipeToolStripMenuItem.Name = "AddPipeToolStripMenuItem"
        Me.AddPipeToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.AddPipeToolStripMenuItem.Text = "Add Pipe"
        '
        'DeletePipeToolStripMenuItem
        '
        Me.DeletePipeToolStripMenuItem.Name = "DeletePipeToolStripMenuItem"
        Me.DeletePipeToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.DeletePipeToolStripMenuItem.Text = "Delete Pipe"
        '
        'PipeClipEditorToolStripMenuItem
        '
        Me.PipeClipEditorToolStripMenuItem.Name = "PipeClipEditorToolStripMenuItem"
        Me.PipeClipEditorToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.PipeClipEditorToolStripMenuItem.Text = "Pipe Clip Editor"
        '
        'ToolStripButtonAddMatSwopSys
        '
        Me.ToolStripButtonAddMatSwopSys.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonAddMatSwopSys.Image = CType(resources.GetObject("ToolStripButtonAddMatSwopSys.Image"), System.Drawing.Image)
        Me.ToolStripButtonAddMatSwopSys.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonAddMatSwopSys.Name = "ToolStripButtonAddMatSwopSys"
        Me.ToolStripButtonAddMatSwopSys.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonAddMatSwopSys.Text = "ToolStripButton10"
        Me.ToolStripButtonAddMatSwopSys.ToolTipText = "Add Material Swop System"
        '
        'ToolStripButtonMatSwopEditor
        '
        Me.ToolStripButtonMatSwopEditor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonMatSwopEditor.Image = CType(resources.GetObject("ToolStripButtonMatSwopEditor.Image"), System.Drawing.Image)
        Me.ToolStripButtonMatSwopEditor.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonMatSwopEditor.Name = "ToolStripButtonMatSwopEditor"
        Me.ToolStripButtonMatSwopEditor.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonMatSwopEditor.Text = "ToolStripButton11"
        Me.ToolStripButtonMatSwopEditor.ToolTipText = "Material Swop Editor"
        '
        'ToolStripButtonSwopSystem
        '
        Me.ToolStripButtonSwopSystem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonSwopSystem.Image = CType(resources.GetObject("ToolStripButtonSwopSystem.Image"), System.Drawing.Image)
        Me.ToolStripButtonSwopSystem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonSwopSystem.Name = "ToolStripButtonSwopSystem"
        Me.ToolStripButtonSwopSystem.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonSwopSystem.Text = "ToolStripButton12"
        Me.ToolStripButtonSwopSystem.ToolTipText = "Swop System"
        '
        'Main
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(624, 341)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.lblTotal)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Menu = Me.MainMenu1
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Material Schedule - Noname.sch"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    'Private ABG As Aspect.AboveGrd
    Private MP As MarleyPart
    Private scanner As Aspect.Scanner
    Private configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
    Private dbLocation As String
    Private sqldblocation As String
    Private Const MessageCaption As String = "Material Schedule 2021"
    Private Const Company As String = "Marley Plumbing && Drainage Ltd."

    Private Sub mnuOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOpen.Click, ToolStripButtonOpen.Click
        Try
            checkEditStatus()
            openSchedule()
        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub



    Private Sub mnuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSave.Click, ToolStripButtonSave.Click
        Try
            If Me.Text.IndexOf("Noname") = -1 Then
                saveSchedule(Me.Text)
            Else
                saveScheduleAs("Noname")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

    End Sub


    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            'dbLocation = CType(configurationAppSettings.GetValue("PartsDataLocation", GetType(System.String)), String)
            dbLocation = dbManager.dbLocation
            If dbLocation = "" Then
                'get new location
                updatePartsLocation()
            End If
            sqldblocation = dbManager.sqldbLocation

            'ABG = New AboveGrd(dbManager.connectionString)
            ' MP = New MarleyPart(dbManager.connectionString)
            MP = New MarleyPart(sqldblocation)

        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
#Region "Private Members"
    Private Sub switchMenus(ByVal state As Boolean)
        Me.mnuSave.Enabled = state
        Me.mnuSaveAs.Enabled = state
        Me.mnuPrint.Enabled = state
        ToolStripButtonSave.Enabled = state
        ToolStripButtonPrint.Enabled = state

    End Sub


    Private Sub resetGridColumns2()
        With DataGridView1

            .Columns(0).Width = 100
            .Columns(0).HeaderText = "Part No."
            .Columns(1).Width = 50
            .Columns(1).HeaderText = "No. Off"
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns(2).HeaderText = "Description"
            .Columns(2).ReadOnly = True
            .Columns(3).HeaderText = "Price"
            .Columns(3).Width = 75
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(3).DefaultCellStyle.Format = "c"
            .Columns(4).HeaderText = "Cost"
            .Columns(4).Width = 75
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(4).DefaultCellStyle.Format = "c"
            .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(4).ReadOnly = True
        End With
    End Sub
    Private Sub openSchedule()

        Dim filename As String
        Dim fopenDia As New System.Windows.Forms.OpenFileDialog
        fopenDia.Filter = "Schedule Files|*.sch|All files|*.*"
        fopenDia.Title = "Open Above Ground Take-Off File"

        If fopenDia.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = fopenDia.FileName
            DataGridView1.DataSource = MP.openSheduleFile(filename)
            resetGridColumns2()
            Me.Text = filename
            ToolStripStatusLabelTotal.Text = MP.getTotal()
            'enable menus and toolbar buttons
            switchMenus(True)
        End If
    End Sub

    Private Function saveScheduleAs(ByVal filename As String) As String
        Dim fSaveDia As New System.Windows.Forms.SaveFileDialog
        fSaveDia.Filter = "Schedule Files|*.sch|All files|*.*"
        fSaveDia.DefaultExt = ".sch"
        fSaveDia.Title = "Save Above Ground Take-Off File"
        fSaveDia.FileName = filename
        If fSaveDia.ShowDialog = Windows.Forms.DialogResult.OK Then
            saveSchedule(fSaveDia.FileName)
            filename = fSaveDia.FileName
        End If
        Return filename
    End Function
    Private Sub saveSchedule(ByVal filename As String)
        MP.saveScheduleFile(filename)

    End Sub
#End Region

    Private Sub mnuSaveAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSaveAs.Click
        Try
            Me.Text = saveScheduleAs(Me.Text)

        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        Try
            Dim frmAbout As New About
            frmAbout.strCompany = Company
            frmAbout.ShowDialog()

        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try
    End Sub

    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Me.Close()
    End Sub


    Private Sub mnuBoxScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBoxScan.Click, ToolStripButtonScanArea.Click

        Try
            checkEditStatus()
            Dim sc As New Aspect.Scanner(dbManager.connectionString)

            ' Dim partcodes As System.Collections.Generic.SortedList(Of String, Integer) = sc.scanArea
            Dim partcodes As System.Collections.Generic.SortedList(Of String, String) = sc.scanArea

            If IsNothing(partcodes) = True Then Exit Sub

            displayPartList(partcodes)
            switchMenus(True)
            Me.Text = "Material Schedule - Noname.sch"
        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        AppActivate(Me.Text)
    End Sub

    Private Sub mnuScanAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanAll.Click, ToolStripButtonScanDrawing.Click
        Try
            'Dim t As Date = Date.Now

            checkEditStatus()
            'Dim sc As New Aspect.Scanner(dbManager.connectionString)
            Dim sc As New Aspect.Scanner(dbManager.sqlconnectionString)
            '  Dim partcodes As System.Collections.Generic.SortedList(Of String, Integer) = sc.scanDrawing()
            Dim partcodes As System.Collections.Generic.SortedList(Of String, String) = sc.scanDrawing()
            '
            If IsNothing(partcodes) = True Then Exit Sub

            displayPartList(partcodes)

            switchMenus(True)
            Me.Text = "Material Schedule - Noname.sch"
            'MessageBox.Show(Date.Now.Subtract(t).ToString)
        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        AppActivate(Me.Text)
    End Sub

    Private Sub mnuScanAllDrawings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanAllDrawings.Click, ToolStripButtonScanAllDrawings.Click
        Try
            checkEditStatus()
            Dim sc As New Aspect.Scanner(dbManager.sqlconnectionString)
            'sc.scanAllDrawings()
            '  Dim partcodes As System.Collections.Generic.SortedList(Of String, Integer) = sc.scanAllDrawings
            Dim partcodes As System.Collections.Generic.SortedList(Of String, String) = sc.scanAllDrawings

            If IsNothing(partcodes) = True Then Exit Sub

            displayPartList(partcodes)
            switchMenus(True)
            Me.Text = "Material Schedule - Noname.sch"
        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        AppActivate(Me.Text)
    End Sub


    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click, ToolStripButtonPrint.Click
        Try
            printSchedule2()
        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub



    Private Sub mnuNoOff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNoOff.Click, ToolStripButtonSetNoOff.Click
        setNoOff()
        AppActivate(Me.Text)
    End Sub
    Private Sub printSchedule2()
        Dim frm As New PrintHeaders
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim frm2 As New schedulePrint

            Dim p As New System.Collections.Generic.List(Of Microsoft.Reporting.WinForms.ReportParameter)
            p.Add(New Microsoft.Reporting.WinForms.ReportParameter("title1", frm.txtLine1.Text))
            p.Add(New Microsoft.Reporting.WinForms.ReportParameter("title2", frm.txtLine2.Text))
            p.Add(New Microsoft.Reporting.WinForms.ReportParameter("title3", frm.txtLine3.Text))
            frm2.ReportViewer1.LocalReport.SetParameters(p)
            frm2.ReportViewer1.LocalReport.EnableExternalImages = True

            Dim dt As DataTable = MP.getPartList

            Dim dv As New DataView(dt)
            dv.Sort = "partCode"
            frm2.PartBindingSource.DataSource = dv
            frm2.ShowDialog()
        End If


    End Sub


    Private Sub setNoOff()
        Try
            Dim frmNo As New NoOff
            frmNo.ShowDialog()

            If frmNo.DialogResult = DialogResult.OK Then
                Dim no As Long = frmNo.NoValue
                frmNo.Close()
                MP.setNumberOff(no)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub



    Private Sub mnuPartsDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPartsDB.Click
        updatePartsLocation()
    End Sub

    Private Sub updatePartsLocation()
        Try
            Dim setdb As New locateDB
            'get current setting from config

            dbLocation = dbManager.sqldbLocation
            setdb.PartsLocation = dbLocation
            setdb.ShowDialog()
            If setdb.DialogResult = DialogResult.OK Then
                dbManager.sqldbLocation = setdb.PartsLocation
            End If




        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub writeNewPartsLocation(ByVal location As String)

        Dim path As String = System.Reflection.Assembly.GetExecutingAssembly().Location
        path = path & ".config"
        Dim doc As XmlDocument = New XmlDocument
        doc.Load(path)
        Dim settings As XmlNodeList = doc.GetElementsByTagName("add")
        Dim setting As XmlNode
        Dim att As XmlAttribute
        For Each setting In settings
            For Each att In setting.Attributes
                If att.Name = "key" And att.Value = "PartsDataLocation" Then
                    setting.Attributes("value").Value = location
                    doc.Save(path)
                    Exit For
                End If
            Next
        Next

    End Sub

    Private Sub mnuAspect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAspect.Click
        Dim cc As New Contact
        cc.ShowDialog()
    End Sub

    Private Sub Main_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        checkEditStatus()
    End Sub
    Private Sub checkEditStatus()
        If MP.isEdited Then
            If MessageBox.Show("Current Schedule File is not saved" & vbCrLf & "Do you wish to save file before continuing", MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes Then
                'ABG.dsList = CType(DataGridView1.DataSource, DataView).Table.DataSet

                Try
                    If Me.Text.IndexOf("Noname") = -1 Then
                        saveSchedule(Me.Text)
                    Else
                        saveScheduleAs("Noname")
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try
            End If

        End If
    End Sub


    Private Sub mnuPriceUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPriceUpdate.Click
        Try
            MP.UpdatePriceList()
            MessageBox.Show("Price Update Completed Sucessfully", MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show(ex.Message, MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

    End Sub

    Private Sub MenuItemAssembly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAssembly.Click, AssemblyEditorToolStripMenuItem.Click
        Dim frm As New ComponetAssembly
        frm.ShowDialog()
    End Sub

    Private Sub MenuItemClipEditor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemClipEditor.Click, ToolStripButtonPipeClipEditor.Click, PipeClipEditorToolStripMenuItem.Click
        Dim frm As New ClipEditor
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then

        End If
    End Sub

    Private Sub MenuItemAddSwopSystem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAddSwopSystem.Click, ToolStripButtonAddMatSwopSys.Click
        Dim frm As New AddNewMaterialSwopSystem
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then

        End If
    End Sub


    Private Sub MenuItemMaterialSwopEditor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemMaterialSwopEditor.Click, ToolStripButtonMatSwopEditor.Click
        Dim frm As New MaterialSwop
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then

        End If
    End Sub

    Private Sub MenuItemSwopSystem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemSwopSystem.Click, ToolStripButtonSwopSystem.Click
        Dim frm As New SwopSystem
        If DataGridView1.Rows.Count = 0 Then
            MsgBox("No schedule to swap!!")
            Exit Sub
        End If
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim sw As New Aspect.MarleySwopSystem(dbManager.sqlconnectionString)
            DataGridView1.DataSource = sw.swopScheduleSystem(DataGridView1.DataSource, frm.lstFromSystem.SelectedItem, frm.lstToSystem.SelectedItem)
        End If
    End Sub




    Private Sub DataGridView1_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellEndEdit
        Select Case e.ColumnIndex
            Case 0 'part code changed
                Dim newValue As String = DataGridView1.CurrentCell.Value
                DataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit)

                Dim thispart As partDetails
                thispart.Code = newValue
                thispart = MP.getPartDetails(thispart)
                DataGridView1.CurrentRow.Cells("description").Value = thispart.Description
                If thispart.Price > 0 Then
                    DataGridView1.CurrentRow.Cells("price").Value = thispart.Price
                Else
                    DataGridView1.CurrentRow.Cells("price").Value = 0
                End If
                If DataGridView1.CurrentRow.Cells("nooff").Value Is DBNull.Value Then
                    DataGridView1.CurrentRow.Cells("nooff").Value = 1
                End If

        End Select

        DataGridView1.CurrentRow.Cells("cost").Value = DataGridView1.CurrentRow.Cells("nooff").Value * DataGridView1.CurrentRow.Cells("price").Value

        MP.isEdited = True

    End Sub





    Private Sub DataGridView1_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles DataGridView1.RowPostPaint
        ToolStripStatusLabelTotal.Text = MP.getTotal()

    End Sub

    Private Sub DataGridView1_UserAddedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles DataGridView1.UserAddedRow
        MP.isEdited = True

    End Sub






    Private Sub DataGridView1_UserDeletedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles DataGridView1.UserDeletedRow
        MP.isEdited = True
    End Sub

    Private Sub MenuItemDeleteMaterialSystem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemDeleteMaterialSystem.Click
        Dim frm As New DeleteMaterialSwopSystem
        frm.ShowDialog()
    End Sub

    Private Sub MenuItemAddAssembly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAddAssembly.Click, AddAssemblyToolStripMenuItem.Click
        Dim frm As New AddAssembly
        frm.ShowDialog()
    End Sub

    Private Sub MenuItemAddPipe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAddPipe.Click, AddPipeToolStripMenuItem.Click
        Dim frm As New AddPipe
        frm.ShowDialog()
    End Sub


    Private Sub MenuItemDeletePipe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemDeletePipe.Click, DeletePipeToolStripMenuItem.Click
        Dim frm As New DeletePipe
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim mp As New Aspect.MarleyPipe(dbManager.sqlconnectionString)
            If MessageBox.Show("You are about to delete a Pipe" & vbCrLf & "Are you sure", "Title", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) Then
                mp.deletePipe(frm.lstPipes.SelectedItem!ID)
            End If
        End If
    End Sub


    Private Sub MenuItemDeleteAssembly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemDeleteAssembly.Click, DeleteAssemblyToolStripMenuItem.Click
        Dim frm As New DeleteAssembly
        frm.ShowDialog()
    End Sub




    Private Sub displayPartList(ByVal partcodes As System.Collections.Generic.SortedList(Of String, String))
        DataGridView1.DataSource = MP.generatePartList(partcodes)
        resetGridColumns2()
        switchMenus(True)
        Me.Text = "Material Schedule - Noname.sch"
    End Sub


    Private Sub MenuItemGetDrgAssembliess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemGetDrgAssembliess.Click
        MP.addAssemblyPartsFromDrawing()
        'scan drawing for assemblies
        'for each assembly
        'add to database with parts list if not already there
    End Sub

    Private Sub StatusStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles StatusStrip1.ItemClicked

    End Sub
End Class
Public Structure printHeaderText
    Public line1 As String
    Public line2 As String
    Public line3 As String
End Structure
